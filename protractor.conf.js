exports.config = {
  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    // 'browserName': 'internet explorer', - special installation needed
    // 'version':'10',
    'browserName': 'chrome'
    //'browserName': 'firefox'
  },
  allScriptsTimeout: 30000,
  baseUrl: 'http://squirreldocuments-d.herokuapp.com',
  // Spec patterns are relative to the current working directly when
  // protractor is called.
  specs: ['test/user-login.js', 'test/unclassifieds-documents.js'],
  //specs: ['test/protractor/buyer-login.js', 'test/protractor/buyer-discover-filter.js'],

  params: {
    credentials: {
      user: {
        email: 'richard@marindepan.se',
        password: 'foo'
      }
    }
  },
  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
