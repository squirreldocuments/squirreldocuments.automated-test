setup:
------
    npm install -g protractor
    webdriver-manager update

run:
-----
    webdriver-manager start
    protractor protractor.conf.js

configure:
----------
You might want to check `protractor.conf.js` for environment-specific
configuration.
