describe('landing page', function() {
  'use strict';
  beforeEach(function() {
    var url = browser.baseUrl + '/unclassifieds';
    browser.get(url);
  });
  it('should display unclassifieds documents', function() {
    expect(element(by.css('.page-heading h2')).getText())
      .toEqual('Unclassified documents');
    expect(element(by.css('.paginate_button.active')).getText())
      .toEqual('1');
    expect(element(by.css('.activeState span')).getText())
      .toEqual('Unclassified');
    var elements = element
      .all(by.repeater('document in documentsUnclassified.documents ' +
      'track by $index | orderBy:\'createdAt\''));
    elements.first().then(function(term) {
      term.element(by.css('.btn.btn-primary.btn-xs')).then(function(div) {
        div.click();
        expect(browser.getCurrentUrl()).toContain('/unclassified/');
      });
    });
  });
});

