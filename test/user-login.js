describe('landing page', function() {
  beforeEach(function() {
    browser.get(browser.baseUrl);
  })

  it('should not login', function() {
    expect(element(by.css('.loginscreen h3')).getText()).toEqual('Welcome to IN+');
  });

  it('should not login', function() {

    browser.findElement(by.css('button[type="submit"]')).click();
    browser.waitForAngular();

    expect(browser.getCurrentUrl()).toContain('login');
    browser.findElement(by.model('login.user.email')).sendKeys(browser.params.credentials.user.email + 'wrong');
    browser.findElement(by.css('button[type="submit"]')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('login');

    browser.findElement(by.model('login.user.password')).sendKeys(browser.params.credentials.user.password + 'wrong');
    browser.findElement(by.css('button[type="submit"]')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('login');
  });

  it('should login', function() {
    browser.findElement(by.model('login.user.email')).sendKeys(browser.params.credentials.user.email);
    browser.findElement(by.model('login.user.password')).sendKeys(browser.params.credentials.user.password);
    browser.findElement(by.css('button[type="submit"]')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toContain('main');
  });

});

